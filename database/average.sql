-- 
SELECT id, temperature, AVG(temperature) OVER ( ORDER BY id ROWS BETWEEN 86400 PRECEDING AND 1 PRECEDING) AS avg_temperature FROM $my_table;

-- Exportation de la table au format CSV
COPY temperature TO '/var/lib/clickhouse/tmp/avg_temperature.csv' DELIMITER ',' CSV HEADER;