-- Création de la table
CREATE OR REPLACE TABLE temperature (
    id UInt64,
    temperature Int8
)
ENGINE = MergeTree
ORDER BY id;

-- DROP TABLE temperature