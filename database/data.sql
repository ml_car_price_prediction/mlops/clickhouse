-- Remplissage de la table avec 50 millions de lignes
INSERT INTO temperature (temperature)
SELECT floor(random() * (40 - (-10) + 1) + (-10))
FROM generate_series(1, 50000000);

-- Exportation de la table au format CSV
COPY temperature TO '/var/lib/clickhouse/tmp/temperature.csv' DELIMITER ',' CSV HEADER;