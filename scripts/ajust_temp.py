import random
import time
from clickhouse_driver import Client

client = Client(host='localhost')

start_time = time.time()

for temp in range(1, 10001):
    delta = random.choice([-1, 0, 1])
    new_temp = temp + delta
    client.execute("UPDATE temperatures SET temp = %s WHERE id = %s", (new_temp, temp))

end_time = time.time()

print("Temps d'exécution : {} secondes".format(end_time - start_time))
