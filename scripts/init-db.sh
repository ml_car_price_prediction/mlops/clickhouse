#!/bin/bash
db_name='test'
db_user='test'
my_table='temperature'
my_csv='/var/lib/clickhouse/tmp/temperature.csv'

# set -e
# clickhouse client -n <<-EOSQL
#     CREATE DATABASE docker;
#     CREATE TABLE docker.docker (x Int32) ENGINE = Log;
# EOSQL

function import_csv() {
  cat $my_csv | clickhouse-client --query="INSERT INTO $my_table FORMAT CSVWithNames";
}

function log() {
  printf "Log [%s]: %s\n" "$(date -u +"%Y-%m-%dT%H:%M:%SZ")" "$*"
}

function count_rows() {
    clickhouse-client --query="SELECT count(*) FROM $my_table"
}

function test() {
    clickhouse-client --query="SELECT id, temperature, AVG(temperature) OVER ( ORDER BY id ROWS BETWEEN 86400 PRECEDING AND 1 PRECEDING) AS avg_temperature FROM $my_table WHERE id > 49900000"
}

log "start import csv"
import_csv

log "start counting rows"
log "$(count_rows) rows found"

test

log "all done"